package main

import (
	"bytes"
	"log"
	"strings"
	"testing"
)

func TestStart(t *testing.T) {
	// Create the buffer to redirect the log output to
	var buffer bytes.Buffer

	// Set the log output to the new buffer
	log.SetOutput(&buffer)

	// Call method we are testing
	start()

	// Check that the buffer contains the expected log output
	// We use "HasSuffix" since the log output will start with a timestamp
	if !strings.HasSuffix(buffer.String(), "Starting storage-service\n") {
		t.Fail()
	}
}
