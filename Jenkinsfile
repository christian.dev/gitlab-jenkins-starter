pipeline {
  agent {
    docker {
      image 'docker'
    }
  }

  options {
   gitLabConnection('GitLab')
  }

  stages {
    stage('Test') {
      parallel {
        stage('Test: webclient-service') {
          agent {
            docker {
              image 'node:lts-alpine'
              // Setting "reuseNod" to "true" is important if we want to use any data created by
              // this stage at a later point again. The default is "false". This would crate a
              // new workspace we can't access from our default workspace.
              reuseNode true
            }
          }
          steps {
            gitlabCommitStatus(name: 'Test: webclient-service') {
              dir(path: 'webclient-service') {
                sh 'yarn'
                sh 'yarn test:unit -ci'
              }
            }
          }
        }
        stage('Test: authentication-service') {
          agent {
            docker {
              image 'openjdk'
              reuseNode true 
            }
          }
          steps {
            gitlabCommitStatus(name: 'Test: authentication-service') {
              dir(path: 'authentication-service') {
                sh './gradlew check'
              }
            }
          }
        }
        stage('Test: storage-service') {
          agent {
            docker {
              image 'golang'
              reuseNode true
            }
          }
          steps {
            gitlabCommitStatus(name: 'Test: storage-service') {
              dir(path: 'storage-service') {
                sh 'go get -u github.com/jstemmer/go-junit-report'
                sh 'mkdir -p build/output'
                sh 'go test ./... -v 2>&1 | go-junit-report > build/output/test-result.xml && go test ./...'
              }
            }
          }
        }
      }
    }
    stage('Build') {
      parallel {
        stage('Build: webclient-service') {
          steps {
            gitlabCommitStatus(name: 'Build: webclient-service') {
              dir(path: 'webclient-service') {
                sh 'docker build --no-cache -f Dockerfile-build -t webclient-service .'
              }
            }
          }
        }
        stage('Build: storage-service') {
          steps {
            gitlabCommitStatus(name: 'Build: storage-service') {
              dir(path: 'storage-service') {
                sh 'docker build --no-cache -f Dockerfile-build -t storage-service .'
              }
            }
          }
        }
        stage('Build: authentication-service') {
          steps {
            gitlabCommitStatus(name: 'Build: authentication-service') {
              dir(path: 'authentication-service') {
                sh 'docker build --no-cache -f Dockerfile-build -t authentication-service .'
              }
            }
          }
        }
      }
    }
  }

  post {
    always {
      gitlabCommitStatus(name: 'Post: always') {
        dir(path: 'webclient-service') {
          junit 'build/output/**.xml'
        }
        dir(path: 'authentication-service') {
          junit 'build/test-results/test/**.xml'
        }
        dir(path: 'storage-service') {
          junit 'build/output/**.xml'
        }
      }
    }
  }
}
